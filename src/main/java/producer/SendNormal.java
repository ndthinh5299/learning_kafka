package producer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class SendNormal {

  public static void main(String[] args) throws ExecutionException, InterruptedException {

    // construct a Kafka producer
    Properties props = new Properties();
    props.put("bootstrap.servers", "localhost:29092");
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

    KafkaProducer<String, String> producer = new KafkaProducer<>(props);

    ProducerRecord<String, String> record = new ProducerRecord<>("employee", "key", "Hello, World!");

    //Sending a Message to Kafka
    try {
      Future<RecordMetadata> recordMetadataFuture =  producer.send(record);
      RecordMetadata recordMetadata = recordMetadataFuture.get();
      System.out.println("Message sent to partition " + recordMetadata.partition() + " with offset " + recordMetadata.offset());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
//      producer.close();
    }

    // Sending a Message Synchronously
    try {
      RecordMetadata recordMetadata = producer.send(record).get();
      System.out.println("Sending a Message Synchronously to partition " + recordMetadata.partition() + " with offset " + recordMetadata.offset());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
//      producer.close();
    }

    //Sending a Message Asynchronously
    Future<RecordMetadata> future = producer.send(record, (recordMetadata, e) -> {
      if (e != null) {
        e.printStackTrace();
      } else {
        System.out.println("Sending a Message Asynchronously to partition " + recordMetadata.partition() + " with offset " + recordMetadata.offset());
      }
    });


  }
}
