package producer;

import java.util.Properties;
import java.util.concurrent.Future;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class SendWithCustomSerialize {

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:29092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", CustomerSerializer.class);
        Producer<String, Customer> producer = new KafkaProducer<>(props);


        Customer customer = new Customer(1, "John@gmail.com");

        ProducerRecord<String, Customer> record = new ProducerRecord<>("employee", "key", customer);

        //Sending a Message to Kafka
        try {
            Future<RecordMetadata> recordMetadataFuture =  producer.send(record);
            RecordMetadata recordMetadata = recordMetadataFuture.get();
            System.out.println("Message sent to partition " + recordMetadata.partition() + " with offset " + recordMetadata.offset());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//      producer.close();
        }

    }
}
